package uz.pdp.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import uz.pdp.model.User;

import java.util.List;
@RequiredArgsConstructor
@Component
public class UserRepository {
    private final JdbcTemplate jdbcTemplate;


    public void addUser(User user) {
        jdbcTemplate.update("insert into users(fullname,phonenumber,password,rolename) values (?,?,?,?)",
                new Object[]{
                        user.getFullname(),
                        user.getPhonenumber(),
                        user.getPassword(),
                        user.getPassword()},
                new BeanPropertyRowMapper<>(User.class));
    }
    public List<User> getUsers(){
        return   jdbcTemplate.query("select * from users",
                new BeanPropertyRowMapper<>(User.class));
    }
    public void changeActiveStatus(boolean active,int id){
        jdbcTemplate.update("update users set active = ? where id = ?",
                new Object[]{active,id},
                new BeanPropertyRowMapper<>(User.class));
    }
}
