package uz.pdp.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import uz.pdp.model.Ads;
import uz.pdp.model.Category;

import java.util.List;

@Component
@RequiredArgsConstructor
public class AdsRepo {
    public final JdbcTemplate jdbcTemplate;
    public void addAds(Ads ads) {
        jdbcTemplate.update("insert into ads(userid,categoryid,title,description,price) values (?,?,?,?,?)",
                new Object[]{
                     ads.getUserid(),
                     ads.getCategoryid(),
                     ads.getTitle(),
                     ads.getDescription(),
                     ads.getPrice()
                },
                new BeanPropertyRowMapper<>(Category.class)
        );
    }
    public List<Ads> getAds(){
      return   jdbcTemplate.query("select * from ads",
                new BeanPropertyRowMapper<>(Ads.class));
    }
}
