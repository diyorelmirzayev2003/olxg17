package uz.pdp.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import uz.pdp.model.Verify;

import java.util.List;
@Component
@RequiredArgsConstructor
public class VerifyRepo {
   public final JdbcTemplate jdbcTemplate;

    public void addVerify(Verify verify) {
        jdbcTemplate.update("insert into ads(userid,code) values (?,?)",
                new Object[]{
                        verify.getUserid(),
                        verify.getCode(),
                },
                new BeanPropertyRowMapper<>(Verify.class)
        );
    }
        public List<Verify> getVerify(){
            return   jdbcTemplate.query("select * from verify",
                    new BeanPropertyRowMapper<>(Verify.class));
    }
}
