package uz.pdp.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import uz.pdp.model.Category;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CategoryRepo {
    private final JdbcTemplate jdbcTemplate;

    public void addCategory(Category category) {
        jdbcTemplate.update("insert into category(name,parentid) values (?,?)",
                new Object[]{
                        category.getName(),
                        category.getParentid()
                },
                new BeanPropertyRowMapper<>(Category.class)
        );
    }
    public List<Category> getCategories(){
       return jdbcTemplate.query("select * from category",
        new BeanPropertyRowMapper<>(Category.class)
        );
    }

}
