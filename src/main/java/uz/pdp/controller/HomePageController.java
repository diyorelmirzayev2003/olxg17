package uz.pdp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import uz.pdp.model.Ads;
import uz.pdp.service.AdsService;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class HomePageController {

    private final AdsService adsService;
    @GetMapping
    public String homePage(Model model){
        List<Ads> adsList = adsService.getAllAds();
        model.addAttribute("allAds",adsList);
        return "homePage";
    }
}
