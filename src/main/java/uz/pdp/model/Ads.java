package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class Ads {
    private Integer id;
    private Integer userid;
    private Integer categoryid;
    private String title;
    private String description;
    private double price;
}
