package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class Verify {
    private Integer id;
    private Integer userid;
    private Integer code;
    private boolean verified;
}
